/*
Copyright 2018 Lee Hornby

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MAINQTCLI_H
#define MAINQTCLI_H

#include <QObject>
#include <QCoreApplication>
#include <QSettings>

class MainQTCLI : public QObject
{
    Q_OBJECT
public:
    explicit MainQTCLI(QObject *parent = nullptr);
    MainQTCLI(QString infile,QString outfile,QString colour, QObject *parent = nullptr);
    enum retcode {noerr, fileerr, othererr};
private:
    QCoreApplication *app;


signals:
    void finished(int returncode = noerr);

public slots:
    void quit(int returncode = noerr);
    void run();
    void aboutToQuitApp();

private:
    QString soutfile,sinfile,scolour;

};

#endif
