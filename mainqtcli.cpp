/*
Copyright 2018 Lee Hornby

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "mainqtcli.h"
#include <QtCore>
#include <QTextStream>
#include <QXmlStreamReader>

MainQTCLI::MainQTCLI(QObject *parent) : QObject(parent)

{
    app = QCoreApplication::instance();
}
MainQTCLI::MainQTCLI(QString infile,QString outfile,QString colour, QObject *parent) : QObject(parent)

{
    app = QCoreApplication::instance();
    sinfile = infile;
    soutfile = outfile;
    scolour = colour;

}

void MainQTCLI::run()
{
    QTextStream cout(stdout);
    QTextStream cerr(stdout);

    QFile file(sinfile);
    if(!file.exists())
    {
        cerr << "File does not exist" << endl;
        quit(MainQTCLI::fileerr);
        return;
    }
    QFile fh_outfile(soutfile);
    if (fh_outfile.open(QIODevice::WriteOnly) == false)
    {
        cerr << "Could not open file for writing" << endl;
        quit(MainQTCLI::fileerr);
        return;
    }


    file.open(QIODevice::ReadOnly);
    QByteArray baData = file.readAll();
    file.close();
    QXmlStreamReader xml(baData);
    xml.setNamespaceProcessing(false);

    QXmlStreamWriter outxml(&fh_outfile);
    outxml.setAutoFormatting(true);

    QString value,fqname,elname;
    int attindex;
    QXmlStreamAttributes attribs;

    bool hasstroke=false, hasfill=false;
    while (!xml.atEnd())
    {
        xml.readNext();

        attribs = xml.attributes();


        if (xml.tokenType()==QXmlStreamReader::StartElement)
        {
            elname = xml.name().toString();
            outxml.writeStartElement(elname);

            hasstroke=false;
            hasfill=false;
            fqname = "stroke";
            hasstroke = attribs.hasAttribute(fqname);
            if(hasstroke == true)
            {
                value = attribs.value(fqname).toString();
                if (value != "none")
                {
                    attindex = attribs.indexOf(QXmlStreamAttribute(fqname,value));
                    attribs.replace(attindex, QXmlStreamAttribute(fqname,scolour) );
                }

            }

            fqname = "fill";
            hasfill = attribs.hasAttribute(fqname);
            if(hasfill == true)
            {
                value = attribs.value(fqname).toString();
                if (value != "none")
                {
                    attindex = attribs.indexOf(QXmlStreamAttribute(fqname,value));
                    attribs.replace(attindex, QXmlStreamAttribute(fqname,scolour) );
                }
            }

            outxml.writeAttributes(attribs);
        }
        else
        {
            if (xml.tokenType()!=QXmlStreamReader::Characters)
            {
                outxml.writeCurrentToken(xml);
            }
        }

    }

    fh_outfile.close();
    if (xml.hasError()) {
        cerr << xml.errorString() << endl;
    }
    else cout << "Wrote to file: " << soutfile;

    quit();
}

void MainQTCLI::quit(int returncode)
{
    if(returncode != MainQTCLI::noerr )
    {
        exit(returncode);
        return;
    }
    else
    {
    emit finished(returncode);
    }
}

void MainQTCLI::aboutToQuitApp()
{

}



