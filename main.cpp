/*
Copyright 2018 Lee Hornby

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <QCoreApplication>
#include <QTimer>
#include <QCommandLineParser>
#include "mainqtcli.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QCoreApplication::setApplicationName("SVGTool");
    QCoreApplication::setApplicationVersion("1.0");
    QCommandLineParser clparser;
    clparser.setApplicationDescription("SVG file colour converter");
    clparser.addHelpOption();
    clparser.addVersionOption();
    clparser.addPositionalArgument("SVG_source", QCoreApplication::translate("main", "SVG file to change."));
    QCommandLineOption targetfile("o","target SVG file.","target","SVGout.svg");
    QCommandLineOption targetcolor("c","target color.","colour","black");
    clparser.addOption(targetfile);
    clparser.addOption(targetcolor);

    clparser.process(a);
    const QStringList args = clparser.positionalArguments();
    if (args.isEmpty() == true) clparser.showHelp(MainQTCLI::othererr);
    QString infile = args[0];
    QString outfile = clparser.value(targetfile);
    QString colour = clparser.value(targetcolor);

    // Initialise main and connect
    MainQTCLI myMain{infile,outfile,colour};
    //     connect up the signals
    QObject::connect(&myMain, SIGNAL(finished()),
                     &a, SLOT(quit()));
    QObject::connect(&a, SIGNAL(aboutToQuit()),
                     &myMain, SLOT(aboutToQuitApp()));

    QTimer::singleShot(10, &myMain, SLOT(run()));
    return a.exec();
}
