# SVGTool

A small command line tool for converting single colour svg files from one colour to another. 

I use it from changing the colour of svg icons for use in Qt.

## Getting Started

Clone or download the repo.
Open SVGTool.pro in qt-creator then run qmake and build.

Run

SVGTool -h for instructions.

### Prerequisites

qt 5.10.1
May work with others but that's what I've used.
Tested with Qt 5.10.1 MSVC2015 and MinGW 32bit kits.
But should work with others.

## Authors

* **Lee Hornby** 



## License

This project is licensed under the GPLv3 License - see the [LICENSE.](LICENSE) file for details.



